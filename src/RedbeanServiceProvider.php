<?php

namespace LaravelRedbean\ORM;

use Auth;
use LaravelRedbean\ORM\Auth\RedbeanUserProvider;
use RedBeanPHP\R;
use Illuminate\Support\ServiceProvider;

class RedbeanServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('redbean', function ($app, array $config) {
            // Set the Redbean Model namespace
            $model = $config['model'];
            $classArray = explode('\\', $model);
            $model = strtolower(array_pop($classArray));
            $namespace = implode('\\', $classArray);

            if (! defined('REDBEAN_MODEL_PREFIX')) {
                define('REDBEAN_MODEL_PREFIX', '\\'.$namespace.'\\');
            }

            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            return new RedbeanUserProvider($app['hash'], $model);
        });

        $this->publishes([
            __DIR__ . '/../config/redbean.php' => config_path('redbean.php'),
        ]);
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $frozen      = config('redbean.frozen');
        $default     = config('database.default');
        $connections = config('database.connections');
        
        if (config('redbean.multiple_connections')) {
            foreach ($connections as $name => $connection) {
                $this->getConnection($name, $connection, $frozen);
            }
        } else {
            $this->getConnection($default, $connections[$default], $frozen);
        }
        
        R::selectDatabase($default);
    }

    private function getConnection($name, $connection, $frozen)
    {
        // auth details
        $user = isset($connection['username']) ? $connection['username'] : null;
        $pass = isset($connection['password']) ? $connection['password'] : null;

        if ($connection['driver'] == 'sqlite') {
            $dns = 'sqlite:'.$connection['database'];
        }

        if ($connection['driver'] == 'mysql' || $connection['driver'] == 'pgsql') {
            $dsn = $connection['driver'].':host='.$connection['host']. ';dbname='.$connection['database'];
        }
        
        try {
            R::addDatabase($name, $dsn, $user, $pass, $frozen);
        } catch (\Exception $e) {
            
        }
    }
}
