<?php

namespace LaravelRedbean\ORM;

use LaravelRedbean\ORM\Auth\Authenticatable;
use LaravelRedbean\ORM\Auth\Passwords\CanResetPassword;
use RedBeanPHP\SimpleModel;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Within the model, $this->bean refers to the bean.
 */
class User extends SimpleModel implements   AuthenticatableContract,
                                            AuthorizableContract,
                                            CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * Invoked by R::load()
     */
    public function open()
    {
    }

    /**
     * Invoked by R::dispense()
     */
    public function dispense()
    {
    }

    /**
     * Invoked by R::store()
     */
    public function update()
    {
    }

    /**
     * Runs after bean is saved to database
     */
    public function after_update()
    {
    }

    /**
     * Invoked by R::trash() 
     */
    public function delete()
    {
    }

    /**
     * Runs after bean is deleted from database
     */
    public function after_delete()
    {
    }
}
