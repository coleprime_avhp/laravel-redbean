<?php

namespace LaravelRedbean\ORM\Auth;

use RedBeanPHP\R;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Contracts\Auth\Authenticatable;

class RedbeanUserProvider implements UserProvider
{
    /**
     * The hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * The Redbean user model.
     *
     * @var string
     */
    protected $model;

    /**
     * Create a new database user provider.
     *
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @return void
     */
    public function __construct(Hasher $hasher, $model)
    {
        $this->hasher = $hasher;
        $this->model  = $model;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $user = R::load($this->model, $identifier);
        
        if ($user === null) {
            return null;
        }
        
        return $user->box();
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = R::findOne($this->model, 'id = ? AND remember_token = ?', [$identifier, $token]);
        
        if ($user === null) {
            return null;
        }
        
        return $user->box();
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->remember_token = $token;
        R::store($user);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $where  = '';
        $params = [];
        $count  = 0;
        foreach ($credentials as $key => $value) {
            if (! Str::contains($key, 'password')) {
                if (!empty($count)) {
                    $where .= ' AND ';
                }

                $name = ':'.$key;
                $where .= $key.' = '.$name;
                $params[$name] = $value;
            }
            $count++;
        }
        
        $user = R::findOne($this->model, $where, $params);
        
        if ($user === null) {
            return null;
        }
        
        return $user->box();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }
}
